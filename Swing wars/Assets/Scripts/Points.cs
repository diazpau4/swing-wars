using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Points : MonoBehaviour
{
    public int ContadorAzul = 0;
    public int ContadorRojo = 0;
    public GameObject UnoRojo, DosRojo, TresRojo, UnoAzul, DosAzul, TresAzul;

    void Start()
    {
        
    }

    
    void Update()
    {
        
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Rojo"))
        {
            ContadorRojo++;
            if (ContadorRojo == 1)
            {
                UnoRojo.SetActive(true);
            }
            else if (ContadorRojo == 2)
            {
                DosRojo.SetActive(true);
            }
            else if (ContadorRojo == 3)
            {
                TresRojo.SetActive(true);
                SceneManager.LoadScene("Ending");
            }

        }


        if (other.gameObject.CompareTag("Azul"))
        {
            ContadorAzul++;
            if (ContadorAzul == 1)
            {
                UnoAzul.SetActive(true);
            }
            else if (ContadorAzul == 2)
            {
                DosAzul.SetActive(true);
            }
            else if (ContadorAzul == 3)
            {
                TresAzul.SetActive(true);
                SceneManager.LoadScene("Ending");
            }
        }
    }
}
